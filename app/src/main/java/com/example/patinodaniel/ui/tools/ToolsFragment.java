package com.example.patinodaniel.ui.tools;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Artista;
import com.example.patinodaniel.vistas.actividades.ActividadMemoriaPrograma;
import com.example.patinodaniel.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ToolsFragment extends Fragment implements View.OnClickListener{
    TextView datos;
    Button boton;
    InputStream input;
    BufferedReader lector;
    ArtistaAdapter adapter;
    RecyclerView recyclerArtista;
    List<Artista> listaArtista;

    private ToolsViewModel toolsViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.activity_actividad_memoria_programa, container, false);

        return root;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        input = getResources().openRawResource(R.raw.archivo_raw);
        lector = new BufferedReader(new InputStreamReader(input));
        datos = (TextView) getActivity().findViewById(R.id.lblMemoriaPrograma);
        boton = (Button) getActivity().findViewById(R.id.btnMemoriaPrograma);
        recyclerArtista = (RecyclerView) getActivity().findViewById(R.id.RecyclerMemoriaPrograma);
        boton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.btnMemoriaPrograma):
                try {
                    listaArtista = new ArrayList<Artista>();
                    String cadena = lector.readLine();
                    String[] artis = cadena.split(";");
                    String[] arrayArchivo;
                    for (int i = 0; i < artis.length; i++) {
                        arrayArchivo = artis[i].split(",");
                        Artista artista = new Artista();
                        artista.setNombres(arrayArchivo[0]);
                        artista.setApellidos(arrayArchivo[1]);
                        artista.setNombreArtistico(arrayArchivo[2]);
                        artista.setPathFoto(Uri.parse(arrayArchivo[3]).toString());
                        listaArtista.add(artista);
                    }
                    adapter = new ArtistaAdapter(listaArtista);
                    recyclerArtista.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter.setOnclickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cargarDialogo(v);
                        }
                    });
                    recyclerArtista.setAdapter(adapter);
                    lector.close();
                }catch (Exception ex){
                    Log.e("error: ", ex.getMessage());
                }
                break;
        }
    }



    private void cargarDialogo(View view) {
        Dialog dlgDetalles = new Dialog(getContext());
        dlgDetalles.setContentView(R.layout.dlg_datos_artistas);
        TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblDatos1);
        TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblDatos2);
        ImageView fotoDetalles= dlgDetalles.findViewById(R.id.imgDlgDetalles);
        nombresDetalles.setText("Nombres: "+ listaArtista.get(recyclerArtista.getChildAdapterPosition(view)).getNombres());
        apellidosDetalles.setText("Apellidos: "+ listaArtista.get(recyclerArtista.getChildAdapterPosition(view)).getApellidos());
        fotoDetalles.setImageURI(Uri.parse(listaArtista.get(recyclerArtista.getChildAdapterPosition(view)).getPathFoto()));
        dlgDetalles.show();
    }
}