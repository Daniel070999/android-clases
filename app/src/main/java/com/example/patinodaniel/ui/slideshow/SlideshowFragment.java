package com.example.patinodaniel.ui.slideshow;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Artista;
import com.example.patinodaniel.vistas.adapter.ArtistaAdapter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SlideshowFragment extends Fragment implements View.OnClickListener{
    TextView datos, cajaFotoPath;
    ImageView imagen;
    EditText cajaNombres, cajaApellidos;
    Button botonEscribir, botonLeer, botonCargarFoto;
    RecyclerView reciclerSD;
    ArtistaAdapter adapter;
    List<Artista> listaArtistas;
    String informacion;

    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        slideshowViewModel =
                ViewModelProviders.of(this).get(SlideshowViewModel.class);
        View root = inflater.inflate(R.layout.activity_actividad_archivos_sd, container, false);

        return root;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        imagen = (ImageView) getActivity().findViewById(R.id.imgCargarFotoSD);
        datos = (TextView)getActivity().findViewById(R.id.lblSD);
        cajaNombres = (EditText) getActivity().findViewById(R.id.txtNombresSD);
        cajaApellidos= (EditText) getActivity().findViewById(R.id.txtApellidosSD);
        botonEscribir = (Button) getActivity().findViewById(R.id.btnAgregarSD);
        botonLeer = (Button) getActivity().findViewById(R.id.btnListarSD);
        botonCargarFoto= (Button) getActivity().findViewById(R.id.btnCargarImgSD);
        reciclerSD = (RecyclerView) getActivity().findViewById(R.id.recyclerViewSD);
        cajaFotoPath = (TextView) getActivity().findViewById(R.id.lblpatchdefotosd);
        botonEscribir.setOnClickListener(this);
        botonLeer.setOnClickListener(this);
        botonCargarFoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCargarImgSD:
                cargarImg();
                break;
            case R.id.btnAgregarSD:
                BufferedWriter bufferedWriter = null;
                FileWriter fileWriter = null;
                try {
                    File file = Environment.getExternalStorageDirectory(); // ruta del SD
                    File ruta = new File(file.getAbsoluteFile(), "archivo3SD.txt");
                    fileWriter = new FileWriter(ruta.getAbsoluteFile(), true);
                    bufferedWriter = new BufferedWriter(fileWriter);
                    bufferedWriter.write(cajaNombres.getText().toString()+","+cajaApellidos.getText().toString()+","+cajaFotoPath.getText().toString()+";");
                    bufferedWriter.close();
                    Log.e("ruta: ",cajaFotoPath.getText().toString());
                    //OutputStreamWriter escritor = new OutputStreamWriter(new FileOutputStream(file));
                    Toast.makeText(getContext(), "Archivo guardado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                break;
            case R.id.btnListarSD:
                try {
                    File ruta = Environment.getExternalStorageDirectory(); // ruta del SD
                    File file = new File(ruta.getAbsoluteFile(), "archivo3SD.txt");
                    listaArtistas = new ArrayList<Artista>();
                    BufferedReader lector = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                    String lineas = lector.readLine();
                    String[] artis = lineas.split(";");
                    String[] arrayArchivo;
                    for (int i = 0; i < artis.length; i++) {
                        arrayArchivo = artis[i].split(",");
                        Artista artista = new Artista();
                        artista.setNombres(arrayArchivo[0]);
                        artista.setApellidos(arrayArchivo[1]);
                        artista.setPathFoto(Uri.parse(arrayArchivo[2]).toString());
                        listaArtistas.add(artista);
                    }
                    adapter = new ArtistaAdapter(listaArtistas);
                    reciclerSD.setLayoutManager(new LinearLayoutManager(getContext()));
                    adapter.setOnclickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cargarDialogo(v);
                        }
                    });
                    reciclerSD.setAdapter(adapter);
                    lector.close();
                }catch (Exception ex){
                    Log.e("Error SD", ex.getMessage());
                }
                break;
        }
    }
    private void cargarDialogo(View view) {
        Dialog dlgDetalles = new Dialog(getContext());
        dlgDetalles.setContentView(R.layout.dlg_datos_artistas);
        TextView nombresDetalles = dlgDetalles.findViewById(R.id.lblDatos1);
        TextView apellidosDetalles = dlgDetalles.findViewById(R.id.lblDatos2);
        ImageView fotoDetalles= dlgDetalles.findViewById(R.id.imgDlgDetalles);
        nombresDetalles.setText("Nombres: "+ listaArtistas.get(reciclerSD.getChildAdapterPosition(view)).getNombres());
        apellidosDetalles.setText("Apellidos: "+ listaArtistas.get(reciclerSD.getChildAdapterPosition(view)).getApellidos());
        fotoDetalles.setImageURI(Uri.parse(listaArtistas.get(reciclerSD.getChildAdapterPosition(view)).getPathFoto()));
        dlgDetalles.show();
    }
    private void cargarImg() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "Seleccione la aplicacion"),10);
    }
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            Uri path = data.getData();
            informacion = path.toString();
            imagen.setImageURI(path);
            cajaFotoPath.setText(informacion);
        }
    }
}