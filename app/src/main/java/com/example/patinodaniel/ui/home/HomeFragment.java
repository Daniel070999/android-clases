package com.example.patinodaniel.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.patinodaniel.MainActivity;
import com.example.patinodaniel.R;
import com.example.patinodaniel.menu_nav;
import com.example.patinodaniel.vistas.actividades.ActividadEnviarParametros;
import com.example.patinodaniel.vistas.actividades.ActividadVolleyTest;
import com.example.patinodaniel.vistas.actividades.ActivityLogin;
import com.example.patinodaniel.vistas.actividades.ActivitySuma;

public class HomeFragment extends Fragment implements View.OnClickListener{
    Button botonLogin, botonSumar, botonParametros, botonJson;
    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.activity_main, container, false);
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        botonLogin = (Button)getActivity().findViewById(R.id.btnLogin);
        botonSumar = (Button)getActivity().findViewById(R.id.btnSumar);
        botonParametros = (Button)getActivity().findViewById(R.id.btnParametro);
        botonJson = (Button)getActivity().findViewById(R.id.btnJson);
        botonLogin.setOnClickListener(this);
        botonSumar.setOnClickListener(this);
        botonParametros.setOnClickListener(this);
        botonJson.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btnLogin:
                intent = new Intent(getContext(), ActivityLogin.class);
                startActivity(intent);
                break;
            case R.id.btnSumar:
                intent = new Intent(getContext(), ActivitySuma.class);
                startActivity(intent);
                break;
            case R.id.btnParametro:
                intent = new Intent(getContext(), ActividadEnviarParametros.class);
                startActivity(intent);
                break;
            case R.id.btnJson:
                intent = new Intent(getContext(), ActividadVolleyTest.class);
                startActivity(intent);
                break;
        }
    }
}