package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ServicioWebVollyAlumno;
import com.example.patinodaniel.controlador.ServicioWebVollyUsuarioPractica;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.modelo.UsuarioPractica;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;
import com.example.patinodaniel.vistas.adapter.UsuarioAdapterPractica;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActividadSWUsuarioVollyPractica extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaProfesion;
    Button botonCrear, botonListar, botonModificar, botonElimianr, botonBuscar;
    TextView datos;
    RequestQueue mQueue;
    RecyclerView recyclerView;
    List<UsuarioPractica> listaUsuairo;
    UsuarioAdapterPractica adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swusuario_volly_practica);
        cargarComponentes();
    }
    private void cargarCajasdeTexto(View view) {
        cajaId.setText(""+listaUsuairo.get(recyclerView.getChildAdapterPosition(view)).getId());
        cajaProfesion.setText(""+listaUsuairo.get(recyclerView.getChildAdapterPosition(view)).getProfesion());
        cajaNombre.setText(""+listaUsuairo.get(recyclerView.getChildAdapterPosition(view)).getNombre());

    }
    private void cargarComponentes() {
        cajaId = findViewById(R.id.txtidusuariovollypractica);
        cajaProfesion = findViewById(R.id.txtprofesionvollypractica);
        cajaNombre = findViewById(R.id.txtnombreusuariovollypractica);

        datos = findViewById(R.id.lblrespuestavollypractica);

        recyclerView = findViewById(R.id.recyclerusuariovollypractica);

        botonCrear = findViewById(R.id.btnguardarusuariovollypractica);
        botonListar = findViewById(R.id.btnlistarusuariovollypractica);
        botonModificar= findViewById(R.id.btnmodificarusuariovollypractica);
        botonElimianr = findViewById(R.id.btneliminarusuariovollypractica);
        botonBuscar = findViewById(R.id.btnbuscarusuariovollypractica);


        botonCrear.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonElimianr.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
    }
    public void onClick(View v) {
        final ServicioWebVollyUsuarioPractica sw = new ServicioWebVollyUsuarioPractica(this);
        switch (v.getId()){
            case R.id.btnlistarusuariovollypractica:
                sw.listarUsuarios(new ServicioWebVollyUsuarioPractica.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {
                        Log.e("error al listar", message);
                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("usuario");
                            listaUsuairo = new ArrayList<UsuarioPractica>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject usuarios = jsonArray.getJSONObject(i);
                                UsuarioPractica usuario = new UsuarioPractica();
                                usuario.setId(usuarios.getString("documento"));
                                usuario.setNombre(usuarios.getString("nombre"));
                                usuario.setProfesion(usuarios.getString("profesion"));

                                listaUsuairo.add(usuario);
                                adapter = new UsuarioAdapterPractica(listaUsuairo);
                                recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWUsuarioVollyPractica.this));
                                adapter.setOnclickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cargarCajasdeTexto(v);
                                    }
                                });
                                recyclerView.setAdapter(adapter);
                                Log.e("usuarios: ", listaUsuairo.toString());

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error al listar", e.getMessage());
                        }
                    }
                });
                break;
            case R.id.btnguardarusuariovollypractica:
                try {
                    final Map<String, String> parameters = new HashMap<>();
                    String documento = cajaId.getText().toString();
                    String nombre = cajaNombre.getText().toString();
                    String profesion = cajaProfesion.getText().toString();
                    parameters.put("documento", documento);
                    parameters.put("nombre", nombre);
                    parameters.put("profesion",profesion);
                    sw.registrarUsuario(parameters);

                }catch (Exception ex){
                    Log.e("error al guardar", ex.getMessage());
                }
                break;
            case R.id.btnmodificarusuariovollypractica:
                try {
                    final Map<String, String> parameters = new HashMap<>();
                    String documento = cajaId.getText().toString();
                    String nombre = cajaNombre.getText().toString();
                    String profesion = cajaProfesion.getText().toString();
                    parameters.put("documento", documento);
                    parameters.put("nombre", nombre);
                    parameters.put("profesion",profesion);
                    sw.modificarUsuario(parameters);
                }catch (Exception ex){

                }
                break;
            case R.id.btnbuscarusuariovollypractica:
                try {
                    UsuarioPractica usuario = new UsuarioPractica();
                    usuario.setId(cajaId.getText().toString());
                    sw.buscarUsuario(usuario, new ServicioWebVollyAlumno.VolleyResponseListener() {
                        @Override
                        public void onError(String message) {
                            Toast.makeText(ActividadSWUsuarioVollyPractica.this, "Usuario no registrado", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Toast.makeText(ActividadSWUsuarioVollyPractica.this, "Usuario encontrado", Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = response.getJSONArray("usuario");
                                listaUsuairo = new ArrayList<UsuarioPractica>();
                                for (int i = 0 ; i < jsonArray.length() ; i++){
                                    JSONObject usuarios = jsonArray.getJSONObject(i);
                                    UsuarioPractica usuario = new UsuarioPractica();
                                    usuario.setId(usuarios.getString("documento"));
                                    usuario.setNombre(usuarios.getString("nombre"));
                                    usuario.setProfesion(usuarios.getString("profesion"));

                                    listaUsuairo.add(usuario);
                                    adapter = new UsuarioAdapterPractica(listaUsuairo);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWUsuarioVollyPractica.this));
                                    adapter.setOnclickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            cargarCajasdeTexto(v);
                                        }
                                    });
                                    recyclerView.setAdapter(adapter);
                                    Log.e("usuarios: ", listaUsuairo.toString());

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("error al listar", e.getMessage());
                            }
                        }
                    });
                }catch (Exception ex){

                }
                break;
            case R.id.btneliminarusuariovollypractica:
                UsuarioPractica usuario = new UsuarioPractica();
                usuario.setId(cajaId.getText().toString());
                sw.eliminarUsuario(usuario);

                break;

        }
    }
}
