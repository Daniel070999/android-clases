package com.example.patinodaniel.vistas.actividades;

import androidx.fragment.app.FragmentActivity;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ControladorMapas;
import com.example.patinodaniel.modelo.Ruta;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

public class ActividadMapa extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private Marker myMarker;
    private Button botonsatelite, botonterreno, botonhibrido, botonruta;
    List<Ruta> listaRutas=new ArrayList<>();
    List<LatLng> puntos=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_mapa);
        cargarComponente();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void cargarComponente() {

        botonsatelite = findViewById(R.id.btnsatelite);
        botonterreno = findViewById(R.id.btnterreno);
        botonhibrido = findViewById(R.id.btnhibrido);
        botonruta = findViewById(R.id.btnruta);

        botonsatelite.setOnClickListener(this);
        botonterreno.setOnClickListener(this);
        botonhibrido.setOnClickListener(this);
        botonruta.setOnClickListener(this);
    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        ControladorMapas controlador =new ControladorMapas(this);
        mMap = googleMap;
        mMap.setMinZoomPreference(15);
        LatLng casa = new LatLng(-4.009079, -79.197188);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(casa));
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
            Dialog dlgDetallesMapa = new Dialog(ActividadMapa.this);
            dlgDetallesMapa.setContentView(R.layout.dlg_descripcion_mapas);
            TextView titulo = dlgDetallesMapa.findViewById(R.id.lbltitulodescripcionmapas);
            TextView descripcion = dlgDetallesMapa.findViewById(R.id.lbldescripcionmapas);
            ImageView imagen= dlgDetallesMapa.findViewById(R.id.imgfotodescripcionmapas);
                for (Ruta rut:listaRutas) {
                    if(marker.getTitle().equals(rut.getTitulo())){
                        titulo.setText(rut.getTitulo());
                        descripcion.setText(rut.getDescripcion());
                        int id = getResources().getIdentifier(rut.getFoto(), "drawable", getPackageName());
                        imagen.setImageResource(id);
                    }
                }
                dlgDetallesMapa.show();
            }
        });

                listaRutas = controlador.rutaMarcas();

        for (Ruta rut:listaRutas) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(rut.getLatitud(),rut.getLongitud())).title(rut.getTitulo()).snippet(rut.getDesc()).icon(BitmapDescriptorFactory.fromBitmap(obtenerIconos(rut.getIcono()))));
            puntos.add(new LatLng(rut.getLatitud(),rut.getLongitud()));
        }
        controlador.circulo(puntos.get(0),mMap);
        controlador.circulo(puntos.get(puntos.size()-1),mMap);
        controlador.lineas(puntos,mMap);

    }


    public Bitmap obtenerIconos(String iconName){
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),getResources().getIdentifier(iconName, "drawable", getPackageName()));
        return imageBitmap;
    }

    private void mostrarPoligono(){
        //para trazar lineas
        PolylineOptions rectangulo = new PolylineOptions()
                .add(new LatLng(-4.032772, -79.201568),
                        new LatLng(-4.032707, -79.202291),
                        new LatLng(-4.031116, -79.202488));

        rectangulo.color(Color.parseColor("#ff0000"));

        mMap.addPolyline(rectangulo);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnsatelite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.btnterreno:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.btnhibrido:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.btnruta:
                mostrarPoligono();
                break;
        }
    }




}


