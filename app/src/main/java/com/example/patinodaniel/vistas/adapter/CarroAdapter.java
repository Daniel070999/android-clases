package com.example.patinodaniel.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Artista;
import com.example.patinodaniel.modelo.Carro;

import java.util.List;

public class CarroAdapter extends RecyclerView.Adapter<CarroAdapter.ViewHolderCarro> implements View.OnClickListener {
    List<Carro> lista;

    public CarroAdapter(List<Carro> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;
    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }

    @NonNull
    @Override
    public CarroAdapter.ViewHolderCarro onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_carro, null);
        view.setOnClickListener(this);
        return new CarroAdapter.ViewHolderCarro(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarroAdapter.ViewHolderCarro holder, int position) {
        holder.cajaMarca.setText(lista.get(position).getMarca());
        holder.cajaModelo.setText(lista.get(position).getModelo());
        holder.cajaPlaca.setText(lista.get(position).getPlaca());
        holder.cajaAnio.setText(""+lista.get(position).getAnio());
    }

    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderCarro extends RecyclerView.ViewHolder {
        TextView cajaPlaca, cajaModelo, cajaMarca, cajaAnio;
        public ViewHolderCarro(@NonNull View itemView) {
            super(itemView);
            cajaPlaca = itemView.findViewById(R.id.lblPlacaItem);
            cajaModelo = itemView.findViewById(R.id.lblModeloItem);
            cajaMarca = itemView.findViewById(R.id.lblMarcaItem);
            cajaAnio = itemView.findViewById(R.id.lblAnioItem);

        }
    }
}
