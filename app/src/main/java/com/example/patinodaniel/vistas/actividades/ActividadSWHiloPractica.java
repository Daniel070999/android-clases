package com.example.patinodaniel.vistas.actividades;



import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ControladorSWhilopractica;
import com.example.patinodaniel.controlador.ControladorServicioHilo;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.modelo.UsuarioPractica;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ControladorServicioHilo;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;
import com.example.patinodaniel.vistas.adapter.UsuarioAdapterPractica;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;

public class ActividadSWHiloPractica extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaProfesion;
    Button botonCrear, botonListar, botonModificar, botonElimianr, botonBuscar;
    TextView datos;
    RequestQueue mQueue;
    RecyclerView recyclerView;
    List<UsuarioPractica> listaUsuario;
    UsuarioAdapterPractica adapter;



    ControladorSWhilopractica sw;
    String host = "http://192.168.2.116/PatinoVasquez";
    String insert = "/wsJSONRegistroMovil.php";
    String get = "/wsJSONConsultarLista.php";
    String getById = "/wsJSONConsultarUsuarioUrl.php";
    String update = "/wsJSONUpdateMovil.php";
    String delete = "/wsJSONDeleteMovil.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swhilo_practica);
        mQueue = Volley.newRequestQueue(this);

        CargarComponentes();
    }

    private void CargarComponentes() {
        cajaId = findViewById(R.id.txtidusuariopractica);
        cajaProfesion = findViewById(R.id.txtprofesionusuariopractica);
        cajaNombre = findViewById(R.id.txtnombreusuariopractica);

        datos = findViewById(R.id.lblrespuestaswhilopractica);

        recyclerView = findViewById(R.id.recyclerusuariopractica);

        botonCrear = findViewById(R.id.btnguardarhilopractica);
        botonListar = findViewById(R.id.btnlistarhilopractica);
        botonModificar= findViewById(R.id.btnmodificarhilopractica);
        botonElimianr = findViewById(R.id.btneliminarhilopractica);
        botonBuscar = findViewById(R.id.btnbuscarhilopractica);


        botonCrear.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonElimianr.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
    }

    private void JsonParse(String cadena) throws JSONException {
        Log.e("json", cadena);
        JSONObject jsonObject = new JSONObject(cadena);
        JSONArray jsonArray = jsonObject.getJSONArray("usuario");
        listaUsuario =new ArrayList<UsuarioPractica>();
        for (int i=0 ; i<jsonArray.length() ; i++){
            JSONObject usuarios = jsonArray.getJSONObject(i);
            UsuarioPractica usuario = new UsuarioPractica();
            usuario.setId(usuarios.getString("documento"));
            usuario.setNombre(usuarios.getString("nombre"));
            usuario.setProfesion(usuarios.getString("profesion"));

            listaUsuario.add(usuario);
            adapter = new UsuarioAdapterPractica(listaUsuario);
            recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWHiloPractica.this));
            adapter.setOnclickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarCajasdeTexto(v);
                }
            });
            recyclerView.setAdapter(adapter);
        }
    }
    private void cargarUsuario(String cadena) throws JSONException {
        JSONObject jsonObject = new JSONObject(cadena);
        JSONArray jsonArray = jsonObject.getJSONArray("usuario");
        listaUsuario = new ArrayList<UsuarioPractica>();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject usuarios = jsonArray.getJSONObject(i);
            UsuarioPractica usuario = new UsuarioPractica();
            usuario.setId(usuarios.getString("documento"));
            usuario.setNombre(usuarios.getString("nombre"));
            usuario.setProfesion(usuarios.getString("profesion"));

            listaUsuario.add(usuario);
            adapter = new UsuarioAdapterPractica(listaUsuario);
            recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWHiloPractica.this));
            adapter.setOnclickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cargarCajasdeTexto(v);
                }
            });
            recyclerView.setAdapter(adapter);
            Log.e("usuarios: ", listaUsuario.toString());


        }
    }

    private void cargarCajasdeTexto(View view) {
        cajaId.setText(""+listaUsuario.get(recyclerView.getChildAdapterPosition(view)).getId());
        cajaProfesion.setText(""+listaUsuario.get(recyclerView.getChildAdapterPosition(view)).getProfesion());
        cajaNombre.setText(""+listaUsuario.get(recyclerView.getChildAdapterPosition(view)).getNombre());

    }

    @Override
    public void onClick(View v) {
        sw = new ControladorSWhilopractica();
        switch (v.getId()){
            case R.id.btnlistarhilopractica:
                try {
                    String cadena = sw.execute(host.concat(get),"1").get();
                    JsonParse(cadena);
                } catch (Exception e) {
                    Log.e("error al listar", e.getMessage());
                }
                break;
            case R.id.btnguardarhilopractica:
                String idUsuario = cajaId.getText().toString();
                String nombreUsuario = cajaNombre.getText().toString();
                String profesionUsuario = cajaProfesion.getText().toString();
                sw.execute(host.concat(insert),"2",idUsuario,nombreUsuario,profesionUsuario);
                Toast.makeText(this, "Usuario registrado", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnbuscarhilopractica:
                try {
                    String documento = cajaId.getText().toString();
                    String cadena = sw.execute(host.concat(getById) + "?documento=" +documento,"3").get();
                    cargarUsuario(cadena);
                    Toast.makeText(this, "Usuario encontrado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error al buscar", ex.getMessage());
                    Toast.makeText(this, "Ingrese un id", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.btneliminarhilopractica:
                try {
                    String id = cajaId.getText().toString();
                    sw.execute(host.concat(delete)+"?documento="+id, "4");
                    Toast.makeText(this, "Usuario eliminado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error","no se pudo eliminar" + ex.getMessage());
                }
                break;
            case R.id.btnmodificarhilopractica:
                try {
                    String id = cajaId.getText().toString();
                    String nombre = cajaNombre.getText().toString();
                    String profesion = cajaProfesion.getText().toString();
                    sw.execute(host.concat(update),"5",id,nombre,profesion);
                    Toast.makeText(this, "Usuario modificado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Toast.makeText(this, "Asegurese de llenar las cajas de texto", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }
}
