package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.HelperProducto;
import com.example.patinodaniel.modelo.Producto;
import com.example.patinodaniel.vistas.adapter.ProductoAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class ActividadProductoHelper extends AppCompatActivity implements View.OnClickListener {

    EditText cajaDescripcion, cajaPrecio, cajaCantidad, cajaCodigo;
    Button botonCrear, botonListar, botonModificar, botonElimianr, botonBuscar, botonEliminarTodo;
    RecyclerView recyclerView;
    HelperProducto helperProducto;
    List<Producto> listaProducto;
    BufferedReader lector;
    ProductoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_producto_helper);
        tomarControl();
    }
    private void tomarControl(){

        cajaDescripcion = findViewById(R.id.txtDescripcionDB);
        cajaPrecio = findViewById(R.id.txtPrecioDB);
        cajaCantidad = findViewById(R.id.txtCantidadDB);
        cajaCodigo = findViewById(R.id.txtCodigoDB);
        recyclerView = findViewById(R.id.recyclerViewDB);
        botonCrear = findViewById(R.id.btnCrearDB);
        botonListar = findViewById(R.id.btnListarDB);
        botonModificar= findViewById(R.id.btnModificarDB);
        botonElimianr = findViewById(R.id.btnEliminarDB);
        botonBuscar = findViewById(R.id.btnBuscarDB);
        botonEliminarTodo = findViewById(R.id.btnEliminarTodoDB);

        botonCrear.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonElimianr.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
                case R.id.btnCrearDB:
            try {
                    HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getWritableDatabase();
                    Producto product = new Producto();
                    product.setDescripcion(cajaDescripcion.getText().toString());
                    product.setCantidad(Integer.parseInt(cajaCantidad.getText().toString()));
                    product.setPrecio(Double.parseDouble(cajaPrecio.getText().toString()));
                    product.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                    conn.insertar(product);
                    db.close();
                Toast.makeText(this, "Guardado exitosamente", Toast.LENGTH_SHORT).show();
            }catch (Exception ex){
                Log.e("error" , ex.getMessage());
            }
            break;
            case R.id.btnListarDB:
                try {
                    final HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getReadableDatabase();
                    listaProducto = conn.getAll();
                    adapter = new ProductoAdapter(listaProducto);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    adapter.setOnclickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cargarCajasdeTexto(v);
                        }
                    });
                    recyclerView.setAdapter(adapter);
                    Log.e("lista: ", adapter.toString());
                    db.close();
                }catch (Exception ex){
                    Log.e("error", ex.getMessage());
                }
                break;
            case R.id.btnModificarDB:
                try {
                    HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getReadableDatabase();
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                    producto.setDescripcion(cajaDescripcion.getText().toString());
                    producto.setCantidad(Integer.parseInt(cajaCantidad.getText().toString()));
                    producto.setPrecio(Double.parseDouble(cajaPrecio.getText().toString()));
                    producto.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                    conn.modificar(producto);
                    db.close();
                    Toast.makeText(this, "Producto Modificado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error",ex.getMessage());
                }
                break;
            case R.id.btnEliminarDB:
                try {
                    HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getReadableDatabase();
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                    conn.eliminar(producto);
                    Toast.makeText(this, "Se elimino el producto", Toast.LENGTH_SHORT).show();
                    db.close();
                }catch (Exception ex){
                    Log.e("error: ", ex.getMessage());
                }
                break;
            case R.id.btnBuscarDB:
                try {
                    HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getReadableDatabase();
                    Producto producto = new Producto();
                    producto.setCodigo(Integer.parseInt(cajaCodigo.getText().toString()));
                    String cod = (cajaCodigo.getText().toString());
                    Log.e("producto: ", cod);
                    listaProducto = conn.getProductByCode(cod);
                    Log.e("codigo: ", listaProducto.toString());
                    adapter = new ProductoAdapter(listaProducto);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(adapter);
                    db.close();
                }catch (Exception ex){
                    Log.e("error: ", ex.getMessage());
                }
                break;
            case R.id.btnEliminarTodoDB:
                try{
                    HelperProducto conn = new HelperProducto(this,"name",null,1 );
                    SQLiteDatabase db = conn.getReadableDatabase();
                    conn.eliminarTodo();
                    db.close();
                    Toast.makeText(this, "Se eliminaron todo los productos", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("Error", ex.getMessage());
                }
                break;
        }
    }
    private void cargarCajasdeTexto(View view) {
       cajaCodigo.setText(""+listaProducto.get(recyclerView.getChildAdapterPosition(view)).getCodigo());
       cajaDescripcion.setText(""+listaProducto.get(recyclerView.getChildAdapterPosition(view)).getDescripcion());
       cajaPrecio.setText(""+listaProducto.get(recyclerView.getChildAdapterPosition(view)).getPrecio());
       cajaCantidad.setText(""+listaProducto.get(recyclerView.getChildAdapterPosition(view)).getCantidad());

       }
}
