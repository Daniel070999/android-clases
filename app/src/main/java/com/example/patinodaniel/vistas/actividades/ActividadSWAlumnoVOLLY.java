package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ServicioWebVollyAlumno;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.modelo.Carro;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;
import com.example.patinodaniel.vistas.adapter.CarroAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActividadSWAlumnoVOLLY extends AppCompatActivity implements View.OnClickListener {

    EditText cajaId, cajaNombre, cajaDireccion;
    Button botonCrear, botonListar, botonModificar, botonElimianr, botonBuscar, botonEliminarTodo;
    TextView datos;
    RequestQueue mQueue;
    RecyclerView recyclerView;
    List<Alumno> listaAlumno;
    AlumnoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swalumno_volly);
        cargarComponentes();
    }

    private void cargarComponentes() {
        cajaId = findViewById(R.id.txtIdAlumnoVolly);
        cajaDireccion = findViewById(R.id.txtDireccionSWvolly);
        cajaNombre = findViewById(R.id.txtNombreSWvolly);

        datos = findViewById(R.id.lbldatosVolly);

        recyclerView = findViewById(R.id.recyclerSWvolly);

        botonCrear = findViewById(R.id.btnGuardarswVolly);
        botonListar = findViewById(R.id.btnListarSWvolly);
        botonModificar= findViewById(R.id.btnModificarSWvolly);
        botonElimianr = findViewById(R.id.btnEliminarswVolly);
        botonBuscar = findViewById(R.id.btnBuscarSWvolly);


        botonCrear.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonElimianr.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        ServicioWebVollyAlumno sw = new ServicioWebVollyAlumno(this);
        switch (v.getId()){
            case R.id.btnListarSWvolly:
                sw.findAllAlumnos(new ServicioWebVollyAlumno.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            listaAlumno = new ArrayList<Alumno>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setIdAlumno(alumnos.getString("idalumno"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("direccion"));

                                int id = alumnos.getInt("idalumno");
                                String nombre = alumnos.getString("nombre");
                                String direccion = alumnos.getString("direccion");

                                listaAlumno.add(alumno);
                                adapter = new AlumnoAdapter(listaAlumno);
                                recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWAlumnoVOLLY.this));
                                adapter.setOnclickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cargarCajasdeTexto(v);
                                    }
                                });
                                recyclerView.setAdapter(adapter);
                                Log.e ("devuelve","id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");
                               // datos.append("id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                break;
            case R.id.btnGuardarswVolly:
                try {
                    Alumno alumno = new Alumno();
                    alumno.setNombre(cajaNombre.getText().toString());
                    alumno.setDireccion(cajaDireccion.getText().toString());
                    sw.insertarStudent(alumno);
                }catch (Exception ex){
                    Toast.makeText(this, "Llenen los campos correspondientes", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnModificarSWvolly:
                try {
                    Alumno alumno = new Alumno();
                    alumno.setNombre(cajaNombre.getText().toString());
                    alumno.setDireccion(cajaDireccion.getText().toString());
                    alumno.setIdAlumno(cajaId.getText().toString());
                    sw.modificarAlumno(alumno);
                }catch (Exception ex){

                }
                break;
            case R.id.btnBuscarSWvolly:
            try {
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(cajaId.getText().toString());
                sw.buscarAlumno(alumno, new ServicioWebVollyAlumno.VolleyResponseListener() {
                    @Override
                    public void onError(String message) {

                    }

                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            JSONObject jsonObject = response.getJSONObject("alumno");
                            Log.e("devuelve", jsonObject.toString());
                            listaAlumno = new ArrayList<Alumno>();
                            Alumno alumno = new Alumno();
                            alumno.setIdAlumno(jsonObject.getString("idAlumno"));
                            alumno.setNombre(jsonObject.getString("nombre"));
                            alumno.setDireccion(jsonObject.getString("direccion"));
                            listaAlumno.add(alumno);
                            adapter = new AlumnoAdapter(listaAlumno);
                            recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWAlumnoVOLLY.this));
                            adapter.setOnclickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    cargarCajasdeTexto(v);
                                }
                            });
                            recyclerView.setAdapter(adapter);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ActividadSWAlumnoVOLLY.this, "Alumno no registrado", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }catch (Exception ex){

            }
                break;
            case R.id.btnEliminarswVolly:
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(cajaId.getText().toString());
                sw.eliminarAlumno(alumno);

                break;

        }
    }
    private void cargarCajasdeTexto(View view) {
        cajaId.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getIdAlumno());
        cajaDireccion.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getDireccion());
        cajaNombre.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getNombre());

    }
}
