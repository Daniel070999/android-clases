package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.example.patinodaniel.R;

public class ActividadSensorLuz extends AppCompatActivity implements SensorEventListener {

    SensorManager manager;
    Sensor sensor;
    TextView cajaluz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_luz);
        cajaluz = findViewById(R.id.lblluz);
        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_LIGHT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, sensor,manager.SENSOR_DELAY_NORMAL);

        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);

        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Float luz = event.values[0];
        cajaluz.setText(luz+"");
        if(luz <= 26.0 ) {
            getWindow().getDecorView().setBackgroundColor(Color.RED);
        }
        if(luz <= 14.0 ){
            getWindow().getDecorView().setBackgroundColor(Color.BLUE);
        }
        if(luz <= 5.0 ){
            getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        }
        }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
