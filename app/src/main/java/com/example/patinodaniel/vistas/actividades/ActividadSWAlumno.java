package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ControladorServicioHilo;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

import static com.android.volley.Request.*;

public class ActividadSWAlumno extends AppCompatActivity implements View.OnClickListener{

    EditText cajaId, cajaNombre, cajaDireccion;
    Button botonCrear, botonListar, botonModificar, botonElimianr, botonBuscar, botonEliminarTodo;
    TextView datos;
    RequestQueue mQueue;
    RecyclerView recyclerView;
    List<Alumno> listaAlumno;
    AlumnoAdapter adapter;

    //definir las url del servicio web
    String host = "http://reneguaman.000webhostapp.com";
    String insert = "/insertar_alumno.php";
    String get = "/obtener_alumnos.php";
    String getById = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";

    ControladorServicioHilo sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swalumno);
        //permite presentar los datos
        mQueue = Volley.newRequestQueue(this);
        cargarComponentes();
    }




    private void cargarAlumno(String url, int id) {
        url += "?idalumno=" + id;
        Log.e("url",url);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                            try {

                                JSONObject jsonObject = response.getJSONObject("alumno");
                                    Log.e("devuelve", jsonObject.toString());
                                    listaAlumno = new ArrayList<Alumno>();
                                    Alumno alumno = new Alumno();
                                    alumno.setIdAlumno(jsonObject.getString("idAlumno"));
                                    alumno.setNombre(jsonObject.getString("nombre"));
                                    alumno.setDireccion(jsonObject.getString("direccion"));
                                    listaAlumno.add(alumno);
                                    adapter = new AlumnoAdapter(listaAlumno);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWAlumno.this));
                                    adapter.setOnclickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cargarCajasdeTexto(v);
                                        }
                                    });
                                    recyclerView.setAdapter(adapter);


                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(ActividadSWAlumno.this, "Alumno no registrado", Toast.LENGTH_SHORT).show();
                            }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    //metodo usado para parsear los datos que se llaman
    private void JsonParse(String url) {
        Log.e("metodo","conectado");
        JsonObjectRequest request = new JsonObjectRequest(Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            listaAlumno = new ArrayList<Alumno>();
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);
                                Alumno alumno = new Alumno();
                                alumno.setIdAlumno(alumnos.getString("idalumno"));
                                alumno.setNombre(alumnos.getString("nombre"));
                                alumno.setDireccion(alumnos.getString("direccion"));

                                int id = alumnos.getInt("idalumno");
                                String nombre = alumnos.getString("nombre");
                                String direccion = alumnos.getString("direccion");

                                listaAlumno.add(alumno);
                                adapter = new AlumnoAdapter(listaAlumno);
                                recyclerView.setLayoutManager(new LinearLayoutManager(ActividadSWAlumno.this));
                                adapter.setOnclickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        cargarCajasdeTexto(v);
                                    }
                                });
                                recyclerView.setAdapter(adapter);
                               Log.e ("devuelve","id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");
                                datos.append("id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

    private void cargarComponentes() {
        cajaId = findViewById(R.id.lblIdSWalumnos);
        cajaDireccion = findViewById(R.id.lbldireccionSWalumnos);
        cajaNombre = findViewById(R.id.lblnombreSWalumno);

        datos = findViewById(R.id.lblDatosSW);

        recyclerView = findViewById(R.id.recyclerSWalumnos);

        botonCrear = findViewById(R.id.btnGruardarSWalumnos);
        botonListar = findViewById(R.id.btnListarSWalumnos);
        botonModificar= findViewById(R.id.btnModificarSWalumnos);
        botonElimianr = findViewById(R.id.btnEliminarUnoSWalumnos);
        botonBuscar = findViewById(R.id.btnBuscarSWalumno);
        botonEliminarTodo = findViewById(R.id.btnEliminarTodoSWalumnos);


        botonCrear.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonElimianr.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
    }

    private void cargarCajasdeTexto(View view) {
        cajaId.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getIdAlumno());
        cajaDireccion.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getDireccion());
        cajaNombre.setText(""+listaAlumno.get(recyclerView.getChildAdapterPosition(view)).getNombre());

    }

    @Override
    public void onClick(View v) {
        sw = new ControladorServicioHilo();
        switch (v.getId()){
            case R.id.btnListarSWalumnos:
                sw.execute(host.concat(get),"1");
                String url = host.concat(get);
                JsonParse(url);
                break;
            case R.id.btnGruardarSWalumnos:
                String nombreAlumno = cajaNombre.getText().toString();
                String direccionAlumno = cajaDireccion.getText().toString();
                sw.execute(host.concat(insert),"2",nombreAlumno,direccionAlumno);
                Toast.makeText(this, "Alumno Guardado", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnBuscarSWalumno:
                try {
                    int id = Integer.parseInt(cajaId.getText().toString());
                    sw.execute(host.concat(getById), "3", String.valueOf(id));
                    String url1 = host.concat(getById);
                    cargarAlumno(url1,id);
                    Toast.makeText(this, "Alumno encontrado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error al buscar", ex.getMessage());
                    Toast.makeText(this, "Ingrese un id", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.btnEliminarUnoSWalumnos:
                try {
                    String id = cajaId.getText().toString();
                    sw.execute(host.concat(delete), "4",id);
                    Toast.makeText(this, "Estudiante eliminado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error","no se pudo eliminar" + ex.getMessage());
                }
                break;
            case R.id.btnModificarSWalumnos:
                try {
                    String id = cajaId.getText().toString();
                    String nombre = cajaNombre.getText().toString();
                    String direccion = cajaDireccion.getText().toString();
                    sw.execute(host.concat(update),"5",id,nombre,direccion);
                    Toast.makeText(this, "Estudiante modificado", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Toast.makeText(this, "Asegurese de llenar las cajas de texto", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnEliminarTodoSWalumnos:

                break;

        }
    }
}
