package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.patinodaniel.R;
import com.example.patinodaniel.controlador.ControladorClimaSW;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ActividadSWClima extends AppCompatActivity implements View.OnClickListener {

    Button listar;
    TextView respuesta;

    RequestQueue mQueue;

    String url1 = "https://samples.openweathermap.org/data/2.5/weather?id=2172797&appid=b6907d289e10d714a6e88b30761fae22";

    ControladorClimaSW sw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_swclima);
        cargarComponentes();

    }

    private void cargarComponentes() {
        respuesta = findViewById(R.id.lblCoordenadasswclima);
        listar = findViewById(R.id.btnListarswClima);
        mQueue = Volley.newRequestQueue(this);

        listar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        sw = new ControladorClimaSW();

        sw.execute(url1, "1");
        String url = url1;
        JsonParse(url);

    }



    private void JsonParse(String url) {
        Log.e("metodo","conectado");
        Log.e("URL", String.valueOf(url));
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("obtiene", response.toString());
                        try {
                            JSONObject jsonObject = response.getJSONObject("coord");
                            double longitud = jsonObject.getDouble("lon");
                            double latitud = jsonObject.getDouble("lat");



                            Log.e("devuelve", "longitud: " + longitud + ", latitud: " + latitud + "\n");

                            JSONArray jsonArray = response.getJSONArray("weather");

                            int id = 0;
                            String main = "";
                            String description = "";
                            String icon = "";
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                 id = jsonObject1.getInt("id");
                                 main = jsonObject1.getString("main");
                                 description= jsonObject1.getString("description");
                                 icon = jsonObject1.getString("icon");

                            }

                                String base = response.getString("base");

                            JSONObject jsonObject1 = response.getJSONObject("main");
                            double temp = jsonObject1.getDouble("temp");
                            double pressure = jsonObject1.getDouble("pressure");
                            double humidity = jsonObject1.getDouble("humidity");
                            double temp_min = jsonObject1.getDouble("temp_min");
                            double temp_max = jsonObject1.getDouble("temp_max");

                            String visi = response.getString("visibility");

                            JSONObject jsonObject2 = response.getJSONObject("wind");
                            double speed = jsonObject2.getDouble("speed");
                            double deg = jsonObject2.getDouble("deg");

                            JSONObject jsonObject3 = response.getJSONObject("clouds");
                            double all = jsonObject3.getDouble("all");

                            String dt = response.getString("dt");

                            JSONObject jsonObject4 = response.getJSONObject("sys");
                            double type = jsonObject4.getDouble("type");
                            double idsys = jsonObject4.getDouble("id");
                            double message = jsonObject4.getDouble("message");
                            String country = jsonObject4.getString("country");
                            double sunrise = jsonObject4.getDouble("sunrise");
                            double sunset = jsonObject4.getDouble("sunset");

                            int  idsuelto = response.getInt("id");
                            String name = response.getString("name");
                            int cod = response.getInt("cod");


                            respuesta.setText("Coordenadas: longitud: "+longitud+", latitud: "+latitud+"\n"+"\n"+
                                 "Clima: id: "+id+", main: "+main+", description: "+description+ ", icon: "+icon+"\n"+"\n"+
                                 "base: "+base+"\n"+"\n"+
                                    "Principal: temperatura: "+ temp+", presion: "+pressure+", humedad: "+humidity+", temp. minima: "+ temp_min+", temp. maxima: "+temp_max+"\n"+"\n"+
                                    "visibilidad: "+ visi+"\n"+"\n"+
                                    "Viento: velocidad: "+speed+", deg:"+deg+"\n"+"\n"+
                                    "Nube: "+all+"\n"+"\n"+
                                    "dt: "+dt+"\n"+"\n"+
                                    "Sys: tipo: "+type+", id: "+idsys+", mensaje: " +message+", continente: "+country+", amanecer: "+sunrise+", ocaso: "+sunset+"\n"+"\n"+
                                    "id: "+idsuelto+"\n"+"\n"+
                                    "nombre: " +name+"\n"+"\n"+
                                    "codigo: " +cod);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("error al listar", e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }
}
