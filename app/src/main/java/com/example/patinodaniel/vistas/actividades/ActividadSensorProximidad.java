package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.text.Layout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.util.Log;
import com.example.patinodaniel.R;

public class ActividadSensorProximidad extends AppCompatActivity implements SensorEventListener {

    SensorManager manager;
    Sensor sensor;
    TextView proximidad;
    LinearLayout color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_sensor_proximidad);
        proximidad = findViewById(R.id.lblproximidad);
        color = findViewById(R.id.layoutproximidad);

        manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensor = manager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.registerListener(this, sensor,manager.SENSOR_DELAY_NORMAL);

        Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.unregisterListener(this);

        Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Float pro;
        pro = event.values[0];

        proximidad.setText(pro+"");
        Log.e("n", String.valueOf(pro));
        if (Double.parseDouble(String.valueOf(pro)) != 5.0) {
            color.setBackgroundColor(Color.parseColor("#0000FF"));
        }else{
            color.setBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        /*
        otra manera de cambiar el color de fondo
        getWindows().getDecorView().setBackgroundColor(Color.red)
         */
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
