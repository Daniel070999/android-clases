package com.example.patinodaniel.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.modelo.UsuarioPractica;

import java.util.List;

public class UsuarioAdapterPractica extends RecyclerView.Adapter<UsuarioAdapterPractica.ViewHolderUsuario> implements View.OnClickListener {

    List<UsuarioPractica> lista;

    public UsuarioAdapterPractica(List<UsuarioPractica> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }

    @NonNull
    @Override
    public ViewHolderUsuario onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_usuario_practica, null);
        view.setOnClickListener(this);
        return new UsuarioAdapterPractica.ViewHolderUsuario(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderUsuario holder, int position) {
        holder.datoNombre.setText(lista.get(position).getNombre());
        holder.datoId.setText(lista.get(position).getId());
        holder.datoProfesion.setText(lista.get(position).getProfesion());

    }

    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }
    @Override
    public int getItemCount() {
        return lista.size();
    }

    public class ViewHolderUsuario extends RecyclerView.ViewHolder {
        TextView datoId,datoNombre,datoProfesion;
        public ViewHolderUsuario(@NonNull View itemView) {
            super(itemView);
            datoId = (TextView) itemView.findViewById(R.id.lblidusuarioitem);
            datoNombre = (TextView) itemView.findViewById(R.id.lblnombreusuarioitem);
            datoProfesion= (TextView) itemView.findViewById(R.id.lblprofesionusuarioitem);
        }
    }
}
