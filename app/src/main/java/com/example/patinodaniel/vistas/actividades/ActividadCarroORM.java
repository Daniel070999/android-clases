package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Carro;
import com.example.patinodaniel.vistas.adapter.CarroAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActividadCarroORM extends AppCompatActivity implements View.OnClickListener {

    EditText cajaMarca, cajaModelo, cajaPlaca, cajaAnio;
    TextView respuesta;
    Button botonGuardar, botonListar, botonModificar, botonEliminar, botonBuscar, botonEliminarTodo;
    RecyclerView recyclerCarro;
    List<Carro> listaCarro;
    CarroAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_carro_orm);
        tomarControl();
    }

    private void tomarControl() {
        cajaMarca = findViewById(R.id.txtMarcaORM);
        cajaModelo = findViewById(R.id.txtModeloORM);
        cajaPlaca = findViewById(R.id.txtPlacaORM);
        cajaAnio = findViewById(R.id.txtanioORM);

        respuesta = findViewById(R.id.lblRespuestaORM);

        recyclerCarro = findViewById(R.id.recyclerCarroORM);

        botonGuardar = findViewById(R.id.btnGuardarORM);
        botonBuscar = findViewById(R.id.btnBuscarORM);
        botonEliminar= findViewById(R.id.btnEliminarunoORM);
        botonModificar = findViewById(R.id.btnModificarORM);
        botonEliminarTodo = findViewById(R.id.btnEliminarTodoORM);
        botonListar = findViewById(R.id.btnListarORM);

        botonGuardar.setOnClickListener(this);
        botonListar.setOnClickListener(this);
        botonEliminarTodo.setOnClickListener(this);
        botonModificar.setOnClickListener(this);
        botonEliminar.setOnClickListener(this);
        botonBuscar.setOnClickListener(this);




    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnGuardarORM:
                try{
                Carro carro = new Carro();
                carro.setPlaca(cajaPlaca.getText().toString());
                carro.setModelo(cajaModelo.getText().toString());
                carro.setMarca(cajaMarca.getText().toString());
                carro.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                carro.save();
                Log.e("guardar: ", carro.toString());
                Toast.makeText(ActividadCarroORM.this, "Guardado Correctamente", Toast.LENGTH_SHORT).show();
                borrarCajas();
                }catch (Exception ex){
                    Log.e("error: ", ex.getMessage());
                }
                break;
            case R.id.btnListarORM:
                try {
                    listaCarro = Carro.getAllCars();
                    adapter = new CarroAdapter(listaCarro);
                    recyclerCarro.setLayoutManager(new LinearLayoutManager(this));
                    adapter.setOnclickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            llenarCajas(v);
                        }
                    });
                    recyclerCarro.setAdapter(adapter);
                    Log.e("presenta: ", listaCarro.toString());
                }catch (Exception ex){
                    Log.e("error al listar", ex.getMessage());
                }
                break;
            case R.id.btnBuscarORM:
                try{
                    listaCarro = new ArrayList<Carro>();
                    String placaCarro = cajaPlaca.getText().toString();
                    Carro carro = Carro.obtenerCarroporPlaca(placaCarro);
                    listaCarro.add(carro);
                    adapter = new CarroAdapter(listaCarro);
                    recyclerCarro.setLayoutManager(new LinearLayoutManager(this));
                    adapter.setOnclickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            llenarCajas(v);
                        }
                    });
                    recyclerCarro.setAdapter(adapter);
                    Log.e("devuelve: ", listaCarro.toString());
                }catch (Exception ex){
                    Log.e("error en buscar: ", ex.getMessage());
                }
                break;
            case R.id.btnEliminarTodoORM:
                try {
                    Carro.eliminarTodo();
                    Toast.makeText(this, "Se eleimino todo", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("error al eliminar todo" , ex.getMessage());
                }
                break;
            case R.id.btnModificarORM:
                try {
                    //modificar utilizando el metodo de buscar
                    Carro carro = new Carro();
                    String placa = cajaPlaca.getText().toString();
                    carro = Carro.obtenerCarroporPlaca(placa);
                    carro.setModelo(cajaModelo.getText().toString());
                    carro.setMarca(cajaMarca.getText().toString());
                    carro.setAnio(Integer.parseInt(cajaAnio.getText().toString()));
                    carro.save();
                    Toast.makeText(this, "Carro modificado", Toast.LENGTH_SHORT).show();
                    borrarCajas();
                }catch (Exception ex){
                    Log.e("error al modificar",ex.getMessage());
                }
                break;
            case R.id.btnEliminarunoORM:
                try {
                    String placa = cajaPlaca.getText().toString();
                    Carro.eliminarUno(placa);
                    Toast.makeText(this, "Auto eliminado", Toast.LENGTH_SHORT).show();
                    borrarCajas();
                }catch (Exception ex){
                    Log.e("errorEliminarUno", ex.getMessage());
                }
                break;

        }
    }
    public void llenarCajas(View view){
        cajaModelo.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(view)).getModelo());
        cajaMarca.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(view)).getMarca());
        cajaPlaca.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(view)).getPlaca());
        cajaAnio.setText(""+listaCarro.get(recyclerCarro.getChildAdapterPosition(view)).getAnio());
    }
    public void borrarCajas(){
        cajaAnio.setText("");
        cajaPlaca.setText("");
        cajaModelo.setText("");
        cajaMarca.setText("");
    }
}
