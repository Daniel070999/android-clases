package com.example.patinodaniel.vistas.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Alumno;

import java.util.List;

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.ViewHolderAlumno> implements View.OnClickListener {

    List<Alumno> lista;

    public AlumnoAdapter(List<Alumno> lista){
        this.lista = lista;
    }
    private View.OnClickListener botonClick;

    @Override
    public void onClick(View v) {
        if(botonClick!= null){
            botonClick.onClick(v);
        }
    }

    @NonNull
    @Override
    public AlumnoAdapter.ViewHolderAlumno onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alumno, null);
        view.setOnClickListener(this);
        return new AlumnoAdapter.ViewHolderAlumno(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AlumnoAdapter.ViewHolderAlumno holder, int position) {
        holder.datoNombre.setText(lista.get(position).getNombre());
        holder.datoDireccion.setText(lista.get(position).getDireccion());
        holder.datoId.setText(""+lista.get(position).getIdAlumno());

    }
    public void setOnclickListener(View.OnClickListener onclickListener){
        this.botonClick = onclickListener;
    }

    @Override
    public int getItemCount() {
        return lista.size();

    }
    public class ViewHolderAlumno extends RecyclerView.ViewHolder {
        TextView datoNombre,datoId,datoDireccion;
        public ViewHolderAlumno(@NonNull View itemView) {
            super(itemView);

            datoId = (TextView) itemView.findViewById(R.id.lblidAlumnoitem);
            datoDireccion = (TextView) itemView.findViewById(R.id.lbldireccionalumnoitem);
            datoNombre = (TextView) itemView.findViewById(R.id.lblnombrealumnoitem);
        }
    }
}
