package com.example.patinodaniel.vistas.actividades;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.patinodaniel.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActividadVolleyTest extends AppCompatActivity {

    Button botonJson;
    private TextView resultadoJson;
    private RequestQueue mQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_volley_test);

        botonJson = findViewById(R.id.btnParseJson);

        mQueue = Volley.newRequestQueue(this);

        resultadoJson = findViewById(R.id.lblRespuestaJSON);
        botonJson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonParse();
            }
        });
    }
    private void JsonParse() {
        String url = "http://reneguaman.000webhostapp.com/obtener_alumnos.php";
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("alumnos");
                            for (int i = 0 ; i < jsonArray.length() ; i++){
                                JSONObject alumnos = jsonArray.getJSONObject(i);

                                int id = alumnos.getInt("idalumno");
                                String nombre = alumnos.getString("nombre");
                                String direccion = alumnos.getString("direccion");

                                resultadoJson.append("id: "+String.valueOf(id)+", nombre: "+nombre+", direccion: "+direccion+"\n");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        mQueue.add(request);
    }

}
