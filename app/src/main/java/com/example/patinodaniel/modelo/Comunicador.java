package com.example.patinodaniel.modelo;

public interface Comunicador {
    public void responder(String datos);
}
