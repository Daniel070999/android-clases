package com.example.patinodaniel.modelo;

public class Alumno {
    String nombre;
    String direccion;
    String idAlumno;
    public Alumno(){

    }

    public Alumno(String nombre, String direccion, String idAlumno) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.idAlumno = idAlumno;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdAlumno()
    {
        return idAlumno;
    }

    public void setIdAlumno(String idAlumno) {
        this.idAlumno = idAlumno;
    }
}
