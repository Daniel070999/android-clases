package com.example.patinodaniel.modelo;


import android.database.Cursor;
import android.service.autofill.SaveCallback;
import android.service.autofill.SaveInfo;
import android.service.autofill.SaveRequest;
import android.util.Log;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;

import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

@Table(name = "carro")

public class Carro extends Model {
    @Column(name = "placa", unique = true)
    String placa;
    @Column(name = "modelo", notNull = true)
    String modelo;
    @Column(name = "marca", notNull = true)
    String marca;
    @Column(name = "anio", notNull = true)
    int anio;

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Carro(String placa, String modelo, String marca, int anio) {
        super();
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.anio = anio;
    }
    public Carro() {
        super();
    }
    public static List<Carro> getAllCars(){
        return new Select().from(Carro.class).execute();
    }


    public static List<Carro> eliminarTodo(){
        return new Delete().from(Carro.class).execute();
    }

    public static Carro obtenerCarroporPlaca(String placa){
        return new Select().from(Carro.class).where("placa=?", placa).executeSingle();
    }
//buscar carro con lista
    /*public static List<Carro> buscarPlaca(String placa){
        List<Carro> lista = new ArrayList<Carro>();
        Carro carro = new Select().from(Carro.class).where("placa=?", placa).executeSingle();
            carro.getModelo();
            carro.getMarca();
            carro.getPlaca();
            carro.getAnio();
        lista.add(carro);
        return lista;
    }
    */
//modificar con listas
    /*public static List<Carro> modificar(String modelo, String marca, int anio, String placa){
        List<Carro> lista = new ArrayList<Carro>();
        Carro carro = new Select().from(Carro.class).where("placa=?", placa).executeSingle();
        carro.setPlaca(placa);
        carro.setModelo(modelo);
        carro.setMarca(marca);
        carro.setAnio(anio);
        carro.save();
        return lista;
    }
     */
    public static List<Carro> eliminarUno(String placa){
        return new Delete().from(Carro.class).where("placa=?",placa).execute();
    }
}
