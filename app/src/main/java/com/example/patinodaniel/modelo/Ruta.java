package com.example.patinodaniel.modelo;

public class Ruta {
    private double latitud;
    private double longitud;
    private String titulo;
    private String desc;
    private String icono;
    String descripcion;
    String foto;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Ruta(){

    }
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public Ruta(String foto, String descripcion, double latitud, double longitud, String titulo, String desc, String icono) {
        this.latitud = latitud;
        this.longitud = longitud;
        this.titulo = titulo;
        this.desc = desc;
        this.icono = icono;
        this.descripcion = descripcion;
        this.foto = foto;
    }
}
