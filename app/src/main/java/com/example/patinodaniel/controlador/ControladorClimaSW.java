package com.example.patinodaniel.controlador;

import android.os.AsyncTask;
import android.util.Log;

import com.example.patinodaniel.vistas.actividades.ActividadSWClima;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ControladorClimaSW extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0]; // esta es la ruta... http://000rene.../obtener_alumnos.php
        //parte 1 para listar alumnos
        if (parametros[1].equals("1")) {
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("URL", String.valueOf(url));
                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }

        return consulta;

    }


}
