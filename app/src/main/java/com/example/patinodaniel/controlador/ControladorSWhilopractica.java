package com.example.patinodaniel.controlador;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.CharBuffer;

import javax.net.ssl.HttpsURLConnection;

public class ControladorSWhilopractica extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0];
        if (parametros[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    conexion.connect();

                }
            } catch (Exception ex) {
                ex.getMessage();
            }
        }else if(parametros[1].equals("2")){
            try{
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.setRequestMethod("POST");
                conexion.connect();
                String cadena ="documento="+parametros[2]+"&nombre="+parametros[3]+"&profesion="+parametros[4];
                Log.e("cadena: ",cadena);
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                escritor.write(cadena);
                escritor.flush();
                escritor.close();

                int codigoRespuesta = conexion.getResponseCode();

                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    conexion.connect();
                }

            }catch (Exception ex){
                Log.e("error al guardar", ex.getMessage());
                ex.getMessage();
            }
            //buscar
        }else if(parametros[1].equals("3")){
            try {
                url = new URL(ruta);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int codigo_respuesta = connection.getResponseCode();

                if (codigo_respuesta == HttpURLConnection.HTTP_OK) {
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("datos: ", consulta);
                }
                connection.disconnect();
            } catch (Exception ex) {
                Log.e("error ", ex.getMessage());

            }
            //eliminar
        }else if(parametros[1].equals("4")) {
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpsURLConnection.HTTP_OK) {
                    InputStream stream = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(stream));
                    consulta = lector.readLine();
                }
                conexion.disconnect();
            } catch (Exception es) {

            }
            //modifica
        }else if(parametros[1].equals("5")){
            try{
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.setRequestMethod("POST");
                conexion.connect();
                String cadena ="documento="+parametros[2]+"&nombre="+parametros[3]+"&profesion="+parametros[4];
                Log.e("cadena: ",cadena);
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                escritor.write(cadena);
                escritor.flush();
                escritor.close();

                int codigoRespuesta = conexion.getResponseCode();

                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    conexion.connect();
                }

            }catch (Exception ex){
                Log.e("error al modificar", ex.getMessage());
                ex.getMessage();
            }
        }
        return consulta;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e("execute", s);
    }
}
