package com.example.patinodaniel.controlador;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.PrecomputedText;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.vistas.actividades.ActividadSWAlumno;
import com.example.patinodaniel.vistas.actividades.ActividadSWAlumnoVOLLY;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServicioWebVollyAlumno   {



    Context context;
    //definir las url del servicio web
    String host = "http://reneguaman.000webhostapp.com";
    String insert = "/insertar_alumno.php";
    String get = "/obtener_alumnos.php";
    String getById = "/obtener_alumno_por_id.php";
    String update = "/actualizar_alumno.php";
    String delete = "/borrar_alumno.php";

    TextView datos;
    String consulta;

    List<Alumno> listaAlumno;
    AlumnoAdapter adapter;


    public ServicioWebVollyAlumno(Context context) {
        this.context = context;
    }


    public void findAllAlumnos(final VolleyResponseListener listener){
        String path = host.concat(get);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);
    }

    public void insertarStudent(Alumno alumno){
        String path = host.concat(insert);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("nombre", alumno.getNombre());
            jsonObject.put("direccion", alumno.getDireccion());

        }catch (Exception ex){

        }
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(context, "Alumno registrado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void modificarAlumno( Alumno alumno){
        String path = host.concat(update);
        Log.e("dato","ur: "+ path);
        final JSONObject  object = new JSONObject();
        try {
            object.put("idalumno",alumno.getIdAlumno());
            object.put("nombre",alumno.getNombre());
            object.put("direccion",alumno.getDireccion());
        }catch (Exception es){
            es.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, path,object, new Response.Listener<JSONObject>(){
            @Override
            public void onResponse(JSONObject response) {
                Log.e("estado", response.toString());
                Toast.makeText(context, "Alumno modificado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error.Response", error.toString());
            }
        });

        AlumnoSingletonVolly.getInstance(context).addToRequestqueue(request);
    }
    public void eliminarAlumno(Alumno alumno){
        String path = host.concat(delete);
        JSONObject  object = new JSONObject();
        try {
            object.put("idalumno",alumno.getIdAlumno());
        }catch (Exception es){
            es.printStackTrace();
        }
        JsonObjectRequest dr = new JsonObjectRequest(Request.Method.POST, path,object,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(context, "Alumno eliminado", Toast.LENGTH_SHORT).show();
                        Log.e("estado", response.toString());

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error al eliminar" , error.getMessage());

                    }
                });
        AlumnoSingletonVolly.getInstance(context).addToRequestqueue(dr);

    }
    public  void buscarAlumno(Alumno alumno,  final VolleyResponseListener listener) {

        String path = host.concat(getById);
        path += "?idalumno="+alumno.getIdAlumno();
        Log.e("url", path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                    listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error al buscar",error.getMessage());

            }
        });
        AlumnoSingletonVolly.getInstance(context).addToRequestqueue(jsonObjectRequest);
        Log.e("retorna", listaAlumno.toString());

    }

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }

}
