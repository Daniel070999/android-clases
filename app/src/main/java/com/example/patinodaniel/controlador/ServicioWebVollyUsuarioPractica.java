package com.example.patinodaniel.controlador;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.patinodaniel.modelo.Alumno;
import com.example.patinodaniel.modelo.UsuarioPractica;
import com.example.patinodaniel.vistas.actividades.ActividadSWUsuarioVollyPractica;
import com.example.patinodaniel.vistas.adapter.AlumnoAdapter;
import com.example.patinodaniel.vistas.adapter.UsuarioAdapterPractica;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServicioWebVollyUsuarioPractica {
    Context context;
    //definir las url del servicio web
    String host = "http://192.168.2.116/PatinoVasquez";
    String insert = "/wsJSONRegistroMovil.php";
    String get = "/wsJSONConsultarLista.php";
    String getById = "/wsJSONConsultarUsuarioUrl.php";
    String update = "/wsJSONUpdateMovil.php";
    String delete = "/wsJSONDeleteMovil.php";

    TextView datos;
    String consulta;

    List<UsuarioPractica> listaUsuario;
    AlumnoAdapter adapter;


    public ServicioWebVollyUsuarioPractica(Context context) {
        this.context = context;
    }


    public void listarUsuarios(final ServicioWebVollyUsuarioPractica.VolleyResponseListener listener){
        String path = host.concat(get);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                listener.onResponse(response);
                Log.e("listado", response.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        UsuarioSingletonVollyPractica.getInstance(context).addToRequestqueue(jsonObjectRequest);
    }

    public String registrarUsuario(final Map<String, String> parameters){
        String path = host.concat(insert);
        StringRequest request = new StringRequest(Request.Method.POST,path, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                Toast.makeText(context, "usuario registrado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                return parameters;
            }
        };
        UsuarioSingletonVollyPractica.getInstance(context).addToRequestqueue(request);
        return consulta;
    }
    public void modificarUsuario(final Map<String,String> parametros){
        String path = host.concat(update);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(context, "usuario modificado", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return parametros;
            }
        };



        UsuarioSingletonVollyPractica.getInstance(context).addToRequestqueue(stringRequest);
    }
    public void eliminarUsuario(UsuarioPractica usuario){
        String path = host.concat(delete);
        path += "?documento="+usuario.getId();
        Log.e("url eliminar",path);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("alumno eliminado", response);
                Toast.makeText(context, "usuario elimindo", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error al eliminar", error.getMessage());
                Toast.makeText(context, "usuario no registrado", Toast.LENGTH_SHORT).show();

            }
        });

        UsuarioSingletonVollyPractica.getInstance(context).addToRequestqueue(stringRequest);

    }
    public  void buscarUsuario(UsuarioPractica usuario,  final ServicioWebVollyAlumno.VolleyResponseListener listener) {

        String path = host.concat(getById);
        path += "?documento="+usuario.getId();
        Log.e("url", path);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, path,null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                listener.onResponse(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error al buscar",error.getMessage());

            }
        });
        UsuarioSingletonVollyPractica.getInstance(context).addToRequestqueue(jsonObjectRequest);
        Log.e("retorna", listaUsuario.toString());

    }

    public interface VolleyResponseListener {
        void onError(String message);

        void onResponse(JSONObject response);
    }

}
