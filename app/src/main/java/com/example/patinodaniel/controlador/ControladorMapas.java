package com.example.patinodaniel.controlador;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import com.example.patinodaniel.R;
import com.example.patinodaniel.modelo.Ruta;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ControladorMapas {
    Context context;
    GoogleMap mMap;


    public ControladorMapas(Context context) {
        this.context = context;
    }


    public List<Ruta> rutaMarcas(){
        List<Ruta> listaRuta = new ArrayList<>();
        InputStream input = context.getResources().openRawResource(R.raw.ruta_map);
        BufferedReader lector = new BufferedReader(new InputStreamReader(input));
        try {

            String cadena = lector.readLine();
            while(cadena != null)
            {
                String[] rutas = cadena.split(";");
                String[] arrayArchivo;
                for (int i = 0; i < rutas.length; i++) {
                    arrayArchivo = rutas[i].split(",");
                    Ruta ruta = new Ruta();
                    ruta.setLatitud(Double.parseDouble(arrayArchivo[0]));
                    ruta.setLongitud(Double.parseDouble(arrayArchivo[1]));
                    ruta.setTitulo(arrayArchivo[2]);
                    ruta.setDesc(arrayArchivo[3]);
                    ruta.setIcono(arrayArchivo[4]);
                    ruta.setDescripcion(arrayArchivo[5]);
                    ruta.setFoto(arrayArchivo[6]);
                    listaRuta.add(ruta);
                }
                cadena = lector.readLine();

            }
            lector.close();
        }catch (Exception ex){
            Log.e("error: ", ex.getMessage());
        }
        return listaRuta;
    }


    public Circle circulo(LatLng latLng, GoogleMap googleMap){
        mMap=googleMap;
        Circle circle = mMap.addCircle(new CircleOptions()
                .center(latLng)
                .radius(25)
                .strokeColor(Color.GREEN)
                .strokeWidth(4)
                .fillColor(Color.GREEN));
        return circle;
    }

    public Polyline lineas(List<LatLng> puntos, GoogleMap googleMap){
        mMap=googleMap;
        Polyline line = mMap.addPolyline(new PolylineOptions()
                .addAll(puntos)
                .width(7)
                .color(Color.RED));
        return line;
    }


}
