package com.example.patinodaniel.controlador;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class UsuarioSingletonVollyPractica {
    private RequestQueue queue;
    private Context context;
    private static UsuarioSingletonVollyPractica miInstancia;

    public UsuarioSingletonVollyPractica(Context context){
        this.context = context;
        queue = getRequestQueue();
    }

    public RequestQueue getRequestQueue(){
        if (queue == null)
            queue = Volley.newRequestQueue(context.getApplicationContext());
        return queue;
    }

    public static synchronized UsuarioSingletonVollyPractica getInstance(Context context){
        if (miInstancia == null){
            miInstancia = new UsuarioSingletonVollyPractica(context);
        }
        return miInstancia;
    }

    public <T> void addToRequestqueue(Request request){
        queue.add(request);
    }
}
