package com.example.patinodaniel.controlador;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ControladorServicioHilo extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... parametros) {
        String consulta = "";
        URL url = null;
        String ruta = parametros[0]; // esta es la ruta... http://000rene.../obtener_alumnos.php
        //parte 1 para listar alumnos
        if (parametros[1].equals("1")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("mensaje", consulta);
                    Log.e("URL", String.valueOf(url));
                    conexion.connect();

                }
            } catch (Exception ex) {
                ex.getMessage();
            }
            //parte 2 para ingresar alumnos
        }else if(parametros[1].equals("2")){
            try{
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("nombre",parametros[2]);
                jsonObject.put("direccion",parametros[3]);
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                escritor.write(jsonObject.toString());
                escritor.flush();
                escritor.close();

                int codigoRespuesta = conexion.getResponseCode();

                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                    conexion.connect();
                }

            }catch (Exception ex){
                ex.getMessage();
            }
        }else if(parametros[1].equals("3")){
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                int codigoRespuesta = conexion.getResponseCode();
                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    InputStream in = new BufferedInputStream(conexion.getInputStream());
                    BufferedReader lector = new BufferedReader(new InputStreamReader(in));
                    consulta += lector.readLine();
                    Log.e("mensaje", consulta);
                    Log.e("URL", String.valueOf(url));
                    conexion.connect();

                }
            } catch (Exception ex) {
                ex.getMessage();
            }
            //eliminar
        }else if(parametros[1].equals("4")) {
            try {
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("idalumno",parametros[2]);

                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                escritor.write(jsonObject.toString());
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();

                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                }
                conexion.disconnect();
            } catch (Exception ex) {
                Log.e("error", ex.getMessage());
            }
        }else if(parametros[1].equals("5")){
            try{
                url = new URL(ruta);
                HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
                conexion.setDoInput(true);
                conexion.setDoOutput(true);
                conexion.setUseCaches(false);
                conexion.connect();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("idalumno",parametros[2]);
                jsonObject.put("nombre",parametros[3]);
                jsonObject.put("direccion",parametros[4]);
                OutputStream os = conexion.getOutputStream();
                BufferedWriter escritor = new BufferedWriter(new OutputStreamWriter(os,"UTF-8"));
                escritor.write(jsonObject.toString());
                escritor.flush();
                escritor.close();
                int codigoRespuesta = conexion.getResponseCode();

                if (codigoRespuesta == HttpURLConnection.HTTP_OK){
                    BufferedReader lector = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
                    consulta += lector.readLine();
                }
                Log.e("estado","modificado");
                conexion.disconnect();
            }catch (Exception ex){
                Log.e("error al modificar", ex.getMessage());
            }
        }
        return consulta;
    }
    @Override
    protected void onPostExecute(String s) {
        //datos.setText(s);
    }
}
